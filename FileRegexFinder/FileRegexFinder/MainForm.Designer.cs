﻿namespace FileRegexFinder.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._txtResults = new System.Windows.Forms.TextBox();
            this._folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this._txtSearchPattern = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._lblSelectedDirectory = new System.Windows.Forms.ToolStripStatusLabel();
            this._mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.directoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._menuSelectDirectory = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._btnFind = new System.Windows.Forms.Button();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._lblRegexPattern = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this._mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _txtResults
            // 
            this._txtResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtResults.Location = new System.Drawing.Point(13, 61);
            this._txtResults.Multiline = true;
            this._txtResults.Name = "_txtResults";
            this._txtResults.ReadOnly = true;
            this._txtResults.Size = new System.Drawing.Size(537, 241);
            this._txtResults.TabIndex = 0;
            // 
            // _txtSearchPattern
            // 
            this._txtSearchPattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSearchPattern.Location = new System.Drawing.Point(59, 35);
            this._txtSearchPattern.Name = "_txtSearchPattern";
            this._txtSearchPattern.Size = new System.Drawing.Size(392, 20);
            this._txtSearchPattern.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._lblSelectedDirectory});
            this.statusStrip1.Location = new System.Drawing.Point(0, 305);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(562, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _lblSelectedDirectory
            // 
            this._lblSelectedDirectory.Name = "_lblSelectedDirectory";
            this._lblSelectedDirectory.Size = new System.Drawing.Size(104, 17);
            this._lblSelectedDirectory.Text = "Selected directory:";
            // 
            // _mainMenuStrip
            // 
            this._mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.directoryToolStripMenuItem});
            this._mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this._mainMenuStrip.Name = "_mainMenuStrip";
            this._mainMenuStrip.Size = new System.Drawing.Size(562, 24);
            this._mainMenuStrip.TabIndex = 3;
            this._mainMenuStrip.Text = "menuStrip1";
            // 
            // directoryToolStripMenuItem
            // 
            this.directoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuSelectDirectory,
            this.saveResultsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.directoryToolStripMenuItem.Name = "directoryToolStripMenuItem";
            this.directoryToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.directoryToolStripMenuItem.Text = "File";
            // 
            // _menuSelectDirectory
            // 
            this._menuSelectDirectory.Name = "_menuSelectDirectory";
            this._menuSelectDirectory.Size = new System.Drawing.Size(155, 22);
            this._menuSelectDirectory.Text = "Select directory";
            this._menuSelectDirectory.Click += new System.EventHandler(this.SelectDirectory);
            // 
            // saveResultsToolStripMenuItem
            // 
            this.saveResultsToolStripMenuItem.Name = "saveResultsToolStripMenuItem";
            this.saveResultsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveResultsToolStripMenuItem.Text = "Save results";
            this.saveResultsToolStripMenuItem.Click += new System.EventHandler(this.SaveResults);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.Exit);
            // 
            // _btnFind
            // 
            this._btnFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnFind.Location = new System.Drawing.Point(475, 33);
            this._btnFind.Name = "_btnFind";
            this._btnFind.Size = new System.Drawing.Size(75, 23);
            this._btnFind.TabIndex = 4;
            this._btnFind.Text = "Find";
            this._btnFind.UseVisualStyleBackColor = true;
            this._btnFind.Click += new System.EventHandler(this.Find);
            // 
            // _lblRegexPattern
            // 
            this._lblRegexPattern.AutoSize = true;
            this._lblRegexPattern.Location = new System.Drawing.Point(12, 38);
            this._lblRegexPattern.Name = "_lblRegexPattern";
            this._lblRegexPattern.Size = new System.Drawing.Size(41, 13);
            this._lblRegexPattern.TabIndex = 5;
            this._lblRegexPattern.Text = "Regex:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 327);
            this.Controls.Add(this._lblRegexPattern);
            this.Controls.Add(this._btnFind);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._mainMenuStrip);
            this.Controls.Add(this._txtSearchPattern);
            this.Controls.Add(this._txtResults);
            this.MainMenuStrip = this._mainMenuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Regex Finder";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this._mainMenuStrip.ResumeLayout(false);
            this._mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _txtResults;
        private System.Windows.Forms.FolderBrowserDialog _folderBrowserDialog;
        private System.Windows.Forms.TextBox _txtSearchPattern;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _lblSelectedDirectory;
        private System.Windows.Forms.MenuStrip _mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem directoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _menuSelectDirectory;
        private System.Windows.Forms.ToolStripMenuItem saveResultsToolStripMenuItem;
        private System.Windows.Forms.Button _btnFind;
        private System.Windows.Forms.SaveFileDialog _saveFileDialog;
        private System.Windows.Forms.Label _lblRegexPattern;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

