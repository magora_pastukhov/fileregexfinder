﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Autofac;
using FileRegexFinder.Common.Helpers;
using FileRegexFinder.Logic.Abstract;

namespace FileRegexFinder.UI
{
    public partial class MainForm : Form
    {
        private const int MaxStringLength = 30;

        private IFinderHelper FinderHelper => IoC.Instance.Resolve<IFinderHelper>();
        private IRegexHelper RegexHelper => IoC.Instance.Resolve<IRegexHelper>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void SelectDirectory(object sender, EventArgs e)
        {
            if (_folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                _lblSelectedDirectory.Text = CompactPath(_folderBrowserDialog.SelectedPath, MaxStringLength);
            }
        }

        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        static extern bool PathCompactPathEx([Out] StringBuilder pszOut, string szPath, int cchMax, int dwFlags);

        public static string CompactPath(string longPathName, int wantedLength)
        {
            StringBuilder sb = new StringBuilder(wantedLength + 1);
            PathCompactPathEx(sb, longPathName, wantedLength + 1, 0);
            return sb.ToString();
        }

        private void Find(object sender, EventArgs e)
        {
            if (ValidateSearchData())
            {
                var result = FinderHelper.Search(_folderBrowserDialog.SelectedPath, _txtSearchPattern.Text);
                _txtResults.Text = string.Join(Environment.NewLine, result.FoundPaths);
            }
        }

        private bool ValidateSearchData()
        {
            var message = new StringBuilder();
            if (string.IsNullOrWhiteSpace(_folderBrowserDialog.SelectedPath))
            {
                message.AppendLine("Select directory to scan.");
            }

            if (string.IsNullOrWhiteSpace(_txtSearchPattern.Text))
            {
                message.AppendLine("Specify regex pattern.");
            }
            else if(!RegexHelper.ValidateRegex(_txtSearchPattern.Text))
            {
                message.AppendLine("Regex format is incorrect");
            }

            if (message.Length > 0)
            {
                MessageBox.Show(message.ToString(), @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void SaveResults(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_txtResults.Text))
            {
                MessageBox.Show(@"No results to save.");
            }

            if (_saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(_saveFileDialog.FileName, _txtResults.Text);
                MessageBox.Show(@"File has been saved.");
            }
        }

        private void Exit(object sender, EventArgs e)
        {
            Close();
        }
    }
}
