﻿using System;
using System.Windows.Forms;
using FileRegexFinder.Common.Helpers;
using FileRegexFinder.Logic;

namespace FileRegexFinder.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IoC.Initialize(new LogicIoCModule());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
