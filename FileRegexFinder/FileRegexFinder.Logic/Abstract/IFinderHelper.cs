﻿using FileRegexFinder.Common.Models;

namespace FileRegexFinder.Logic.Abstract
{
    public interface IFinderHelper
    {
        SearchResultModel Search(string directoryPath, string pattern);
    }
}