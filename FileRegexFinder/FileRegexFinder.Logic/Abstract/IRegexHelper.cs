﻿namespace FileRegexFinder.Logic.Abstract
{
    public interface IRegexHelper
    {
        bool ValidateRegex(string pattern);
    }
}