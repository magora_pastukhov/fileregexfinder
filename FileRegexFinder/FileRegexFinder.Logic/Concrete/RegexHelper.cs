﻿using System;
using System.Text.RegularExpressions;
using FileRegexFinder.Logic.Abstract;

namespace FileRegexFinder.Logic.Concrete
{
    public class RegexHelper : IRegexHelper
    {
        public bool ValidateRegex(string pattern)
        {
            if (string.IsNullOrWhiteSpace(pattern))
            {
                return false;
            }

            try
            {
                Regex.Match("", pattern);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }
    }
}