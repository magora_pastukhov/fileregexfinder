﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using FileRegexFinder.Common.Models;
using FileRegexFinder.Logic.Abstract;

namespace FileRegexFinder.Logic.Concrete
{
    public class FinderHelper : IFinderHelper
    {
        public SearchResultModel Search(string directoryPath, string pattern)
        {
            var regex = new Regex(pattern);
            var entries = Directory.EnumerateFileSystemEntries(directoryPath, "*", SearchOption.AllDirectories)
                .Where(x => x != null && regex.IsMatch(Path.GetFileName(x)));

            return new SearchResultModel
            {
                Directory = directoryPath,
                RegexPattern = pattern,
                FoundPaths = entries.ToArray()
            };
        }
    }
}