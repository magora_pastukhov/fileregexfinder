﻿using Autofac;
using FileRegexFinder.Logic.Abstract;
using FileRegexFinder.Logic.Concrete;

namespace FileRegexFinder.Logic
{
    public class LogicIoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<FinderHelper>().As<IFinderHelper>();
            builder.RegisterType<RegexHelper>().As<IRegexHelper>();
        }
    }
}