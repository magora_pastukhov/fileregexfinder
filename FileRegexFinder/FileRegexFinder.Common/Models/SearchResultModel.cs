﻿namespace FileRegexFinder.Common.Models
{
    public sealed class SearchResultModel
    {
        public string Directory { get; set; }
        public string RegexPattern { get; set; }
        public string[] FoundPaths { get; set; }
    }
}