﻿using Autofac;

namespace FileRegexFinder.Common.Helpers
{
    public static class IoC
    {
        private static volatile IContainer _container;

        private static readonly object Sync = new object();

        public static IContainer Instance
        {
            get
            {
                if (_container != null)
                {
                    return _container;
                }

                lock (Sync)
                {
                    if (_container == null)
                    {
                        _container = new ContainerBuilder().Build();
                    }
                }

                return _container;
            }
        }

        public static void Initialize(params Module[] modules)
        {
            var builder = new ContainerBuilder();
            foreach (var module in modules)
            {
                builder.RegisterModule(module);
            }

            lock (Sync)
            {
                _container = builder.Build();
            }
        }
    }
}